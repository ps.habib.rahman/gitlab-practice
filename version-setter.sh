#!/bin/bash
tstamp=$(date '+%y%m%d')
commit=$(git log -1 --pretty=format:"%h")
snap="SNAPSHOT"
version="${tstamp}-${commit}-${snap}"
echo $version > version.txt
mvn versions:set -DnewVersion=$version
mvn versions:commit
mvn clean install
