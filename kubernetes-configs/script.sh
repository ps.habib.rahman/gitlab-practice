#!/bin/bash
USER=root
PASS=password
mysqladmin -h 'mysql.test.svc.cluster.local' -u$USER -p$PASS processlist ###user should have mysql permission on remote server. Ideally you should use different user than root.
if [[ $? -eq 0 ]] 
then 
	echo "do nothing"
else
	exit 100
fi
