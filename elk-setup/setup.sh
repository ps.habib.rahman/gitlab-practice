#! /bin/bash

helm repo add elastic https://helm.elastic.co
curl -O https://raw.githubusercontent.com/elastic/Helm-charts/master/elasticsearch/examples/minikube/values.yaml
helm install elasticsearch elastic/elasticsearch -f ./values.yaml 
helm install kibana elastic/kibana
helm install metricbeat elastic/metricbeat

kubectl port-forward deployment/kibana-kibana 5601
kubectl port-forward svc/elasticsearch-master 9200
