FROM openjdk:11

RUN mkdir app

COPY target/*.jar app/

WORKDIR app

EXPOSE 8070

ENV JAVA_OPTS=''

ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar *.jar"]
